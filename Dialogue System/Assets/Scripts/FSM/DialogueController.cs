﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueController : StateMachine
{
	public List<string> speakersKey;
	public List<SpeakerData> speakers;

	private void Start()
	{
		ChangeState<LoadDialogueState>();
	}
}
