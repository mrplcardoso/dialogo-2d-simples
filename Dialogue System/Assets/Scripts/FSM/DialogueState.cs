﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DialogueState : State
{
	public DialogueController controller
	{ get { return (DialogueController)stateMachine; } }

	public Dictionary<string, SpeakerData> speakers;
}
