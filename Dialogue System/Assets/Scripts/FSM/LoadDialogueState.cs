﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadDialogueState : DialogueState
{
	private void Start()
	{
		speakers = new Dictionary<string, SpeakerData>();
		for(int i = 0; i < controller.speakersKey.Count; ++i)
		{
			speakers.Add(controller.speakersKey[i], controller.speakers[i]);
		}
	}

	public override void OnEnter()
	{
		for(int i = 0; i < controller.speakersKey.Count; ++i)
		{
			speakers[controller.speakersKey[i]].Load();
		}
		StartCoroutine(NextState());
	}

	//Por questões de assincronia dos métodos da FSM
	//essa coroutina serve para esperar algums milésimos
	//antes de transitar para o próximo estado, afim
	//de dar uma folga de tempo na execução do estado atual
	IEnumerator NextState()
	{
		yield return new WaitForSeconds(0.5f);
		controller.ChangeState<RunDialogueState>();
	}
}
