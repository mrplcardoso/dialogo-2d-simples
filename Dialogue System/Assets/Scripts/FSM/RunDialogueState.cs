﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunDialogueState : DialogueState
{
	float letterInterval;
	float phraseInterval;
	int speakerID;

	private void Start()
	{
		letterInterval = 0.08f;
		phraseInterval = 2f;

		speakers = new Dictionary<string, SpeakerData>();
		for (int i = 0; i < controller.speakersKey.Count; ++i)
		{
			speakers.Add(controller.speakersKey[i], controller.speakers[i]);
		}
	}

	public override void OnEnter()
	{
		speakerID = 0;
		StartCoroutine(WindowMoveIn(controller.speakers[speakerID]));
	}

	/*O métodos a seguir implementam a animação
	 de deslocamento das janelas de dialogo.
	 Como cada janela é referênciada pelo personagem que fala, 
	 fica mais eficiente passando o 'speaker' como parâmetro para
	 dentro dos métodos, e manipulando a janela referente a cada speaker
	 por dentro delas.
	 Todas as janelas se movem baseadas na equação OutCircle().
	 Além disso, cada método também é responsavel pela montagem da janela.*/
	IEnumerator WindowMoveIn(SpeakerData speaker)
	{
		ClearWindow(speaker);

		RectTransform windowTransform = speaker.window.GetComponent<RectTransform>();
		float duration = 1f;
		for(float t = 0; t < duration; t += Time.deltaTime)
		{
			windowTransform.localPosition =
				OutCirc3D(speaker.window.outScene, speaker.window.inScene, duration, t);
			yield return null;
		}
		//A execução da janela fica em espera até se completar todo o processo de diálogo:
		//a janela entrar em cena, exibir todas as mensagens e depois sair de cena
		//Quando a janela sai de cena, essá coroutina continua sua execução
		yield return DisplayMessage(speaker);
		
		//Para transitar nos mónologos, usamos o "speakerID", que sempre
		//avança para o próximo índice depois que um processo de diálogo 
		//terminou de executar
		if (speakerID < controller.speakers.Count - 1)
		{
			++speakerID;
			StartCoroutine(WindowMoveIn(controller.speakers[speakerID]));
		}
	}

	/*O método abaixo é um método responsável exclusivamente
	 por exibir mensagens dos speakers. Cada chamada desse método
	 irá exibir um monólogo (arquivo de diálogo) do speaker em questão.
	 A ideia desse método é varrer as letras de cada linha e adicionar ao texto
	 da janela de dialogo com um leve intervalo de tempo.
	 Além disso, ao fim de cada frase (linha), deve esperar um outro intervalo de tempo
	 até iniciar a linha seguinte.*/
	IEnumerator DisplayMessage(SpeakerData speaker)
	{
		bool endMonologue = false;

		for(int i = 0; i < speaker.messages.Count; ++i)
		{
			speaker.window.speakerText.text = "";
			for (int j = 0; j < speaker.messages[i].Length; ++j)
			{
				if(Input.GetAxisRaw("Cancel") != 0)
				{
					endMonologue = true;
					break;
				}
				if(Input.GetAxisRaw("Jump") != 0)
				{
					speaker.window.speakerText.text = speaker.messages[i];
					break;
				}
				speaker.window.speakerText.text += speaker.messages[i][j];
				yield return new WaitForSeconds(letterInterval);
			}
			if (endMonologue) { break; }
			else
			{ yield return new WaitForSeconds(phraseInterval); }
		}
		if (!endMonologue)
		{ yield return new WaitForSeconds(phraseInterval); }

		yield return WindowMoveOut(speaker);
	}


	IEnumerator WindowMoveOut(SpeakerData speaker)
	{
		RectTransform windowTransform = speaker.window.GetComponent<RectTransform>();
		float duration = 1f;
		for (float t = 0; t < duration; t += Time.deltaTime)
		{
			windowTransform.localPosition =
				OutCirc3D(speaker.window.inScene, speaker.window.outScene, duration, t);
			yield return null;
		}
	}

	void ClearWindow(SpeakerData speaker)
	{
		speaker.window.speakerImage.sprite = speaker.speakerImage;
		speaker.window.speakerText.text = "";
	}

	float OutCirc(float start, float end, float duration, float t)
	{
		t /= duration;
		t--;
		return (end - start) * Mathf.Sqrt(1 - t * t) + start;
	}

	Vector3 OutCirc3D(Vector3 start, Vector3 end, float duration, float t)
	{
		float x = OutCirc(start.x, end.x, duration, t);
		float y = OutCirc(start.y, end.y, duration, t);
		float z = OutCirc(start.z, end.z, duration, t);
		return new Vector3(x, y, z);
	}
}
