﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpeakerData
{
	//Imagem do personagem que será exibida na janela de texto
	public Sprite speakerImage;
	//Referência para janela em que o personagem irá falar
	public UIWindow window;

	TextAsset reader;
	//Caminho do texto aonde fica o dialogo do personagem
	public string souceText;
	public List<string> messages;

	public void Load(string sourcePath)
	{
		souceText = sourcePath;
		Load();
	}

	public void Load()
	{
		reader = Resources.Load<TextAsset>("Dialogues/" + souceText);
		messages = TextAssetToList(reader);
	}

	private List<string> TextAssetToList(TextAsset ta)
	{
		List<string> listToReturn = new List<string>();
		string[] arrayString = ta.text.Split('\n');
		foreach (string line in arrayString)
		{
			listToReturn.Add(line);
		}
		return listToReturn;
	}
}
