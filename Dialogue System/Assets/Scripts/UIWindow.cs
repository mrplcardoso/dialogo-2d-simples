﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWindow : MonoBehaviour
{
	//Determinam a posição da caixa de diálogo
	//dentro e fora de cena
	public Vector3 inScene;
	public Vector3 outScene;

	public Image speakerImage;
	public Text speakerText;

	public void Start()
	{
		/*Para pegar as referências dos elementos que variam de acordo com o speaker
		 é necessário acessar os objetos filhos da janela.
		 No caso do Text, existe apenas um objeto filho com componente Text.
		 No caso do Image, existem dois, e, se olharmos no Inspector, veremos que 
		 o objeto Image desejado é o segundo na hierarquia, portanto,
		 uma forma prática de referêcia-lo, é executando o GetComponents()
		 selecionando o segundo elemento retornado.*/
		speakerText = GetComponentInChildren<Text>();
		speakerImage = GetComponentsInChildren<Image>()[1];
	}
}
